package cn.kopsoft.ems.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.kopsoft.ems.mapper.EmsEquipmentTypeMapper;
import cn.kopsoft.ems.domain.EmsEquipmentType;
import cn.kopsoft.ems.service.IEmsEquipmentTypeService;

/**
 * 设备类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
@Service
public class EmsEquipmentTypeServiceImpl implements IEmsEquipmentTypeService 
{
    @Autowired
    private EmsEquipmentTypeMapper emsEquipmentTypeMapper;

    /**
     * 查询设备类型
     * 
     * @param typeId 设备类型主键
     * @return 设备类型
     */
    @Override
    public EmsEquipmentType selectEmsEquipmentTypeByTypeId(String typeId)
    {
        return emsEquipmentTypeMapper.selectEmsEquipmentTypeByTypeId(typeId);
    }

    /**
     * 查询设备类型列表
     * 
     * @param emsEquipmentType 设备类型
     * @return 设备类型
     */
    @Override
    public List<EmsEquipmentType> selectEmsEquipmentTypeList(EmsEquipmentType emsEquipmentType)
    {
        return emsEquipmentTypeMapper.selectEmsEquipmentTypeList(emsEquipmentType);
    }

    /**
     * 新增设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    @Override
    public int insertEmsEquipmentType(EmsEquipmentType emsEquipmentType)
    {
        emsEquipmentType.setCreateTime(DateUtils.getNowDate());
        return emsEquipmentTypeMapper.insertEmsEquipmentType(emsEquipmentType);
    }

    /**
     * 修改设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    @Override
    public int updateEmsEquipmentType(EmsEquipmentType emsEquipmentType)
    {
        emsEquipmentType.setUpdateTime(DateUtils.getNowDate());
        return emsEquipmentTypeMapper.updateEmsEquipmentType(emsEquipmentType);
    }

    /**
     * 批量删除设备类型
     * 
     * @param typeIds 需要删除的设备类型主键
     * @return 结果
     */
    @Override
    public int deleteEmsEquipmentTypeByTypeIds(String[] typeIds)
    {
        return emsEquipmentTypeMapper.deleteEmsEquipmentTypeByTypeIds(typeIds);
    }

    /**
     * 删除设备类型信息
     * 
     * @param typeId 设备类型主键
     * @return 结果
     */
    @Override
    public int deleteEmsEquipmentTypeByTypeId(String typeId)
    {
        return emsEquipmentTypeMapper.deleteEmsEquipmentTypeByTypeId(typeId);
    }
}
